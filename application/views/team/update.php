
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Update Team</h4>
                            </div>
                            <div class="content">
                                <form method="Post" action="<?php echo base_url('Team_add/process_update'); ?>" enctype="multipart/form-data" >
                                 <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" name="team_id" placeholder="Enter Name" value="<?php echo $data['team_id'] ?>">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="team_name" placeholder="Enter Name" value="<?php echo (set_value('team_name'))?(set_value('team_name')):(isset($data['team_name'])?$data['team_name']:'' ) ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="team_email"  placeholder="Enter Email" value="<?php echo (set_value('team_email'))?(set_value('team_email')):(isset($data['team_email'])?$data['team_email']:'' ) ?>">
                                            </div>
                                            </div>
                                        </div>
                                   

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" name="team_password"  placeholder="Enter Password" value="<?php echo (set_value('team_password'))?(set_value('team_password')):(isset($data['team_password'])?$data['team_password']:'' ) ?>">
                                            </div>
                                            </div>
                                       
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Image</label>
                                                <input type="file" class="form-control"  name="team_img" value="<?php echo (set_value('team_img'))?(set_value('team_img')):(isset($data['team_img'])?$data['team_img']:'' ) ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <input type="text" class="form-control"  name="team_desgnation" placeholder="Designation"value="<?php echo (set_value('team_desgnation'))?(set_value('team_desgnation')):(isset($data['team_desgnation'])?$data['team_desgnation']:'' ) ?>">
                                                </div>
                                            </div>
                                        </div>

                                    <input type="submit" class="btn blue" value="Submit">
                                    <div class="clearfix"></div>
                                </form>
                           