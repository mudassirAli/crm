

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add Team</h4>
                            </div>
                            <div class="content">
                                <form method="Post" action="<?php echo base_url('Team_add/process_add'); ?>" enctype="multipart/form-data" >
                                 <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="team_name" placeholder="Enter Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="team_email"  placeholder="Enter Email" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" name="team_password"  placeholder="Enter Password" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Image</label>
                                                <input type="file" class="form-control"  name="team_img" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Devolpment Catagery</label>
                                                <select class="form-control" name="devlprtype_id">
                                                    <option value="">Select Field</option>
                                                    <?php foreach ($projects as $category) {
                                                   ?>
                                                    <option value="<?php echo $category['devlprtype_id']; ?>"><?php echo $category['devlprtype_name']; ?></option>
                                                    
                                                   <?php  } ?>
                                                </select>
                                            
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <input type="text" class="form-control"  name="team_desgnation" placeholder="Designation" >
                                            </div>
                                        </div>
                                     
                                    </div>

                               


                                    <input type="submit" class="btn blue" value="Submit">
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
         

                </div>
            </div>
        </div>


       