<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
  <div class="page-head">
    <div class="page-toolbar">
    </div>
  </div>
  <div class="row">
    <div class="portlet box blue-hoki">
      <div class="portlet-body form">
        <form class="form-horizontal" action="<?php echo base_url('Project/update');?>" method="POST">
          <div class="form-body">
           <div class="form-group">
            <div class="col-md-4">
            </div>
          </div>
          <?php 
          foreach ($key as $value) {
            $id = $value['project_id'];
            ?>
            <input type="hidden" value="<?php echo $id; ?>" name="project_id">
            <div class="form-group">
              <label class="col-md-3 control-label">Project Title</label>
              <div class="col-md-4">
                <input type="text" value="<?php echo $value['project_title']?>" class="form-control" placeholder="Enter Project Name" name="pname" required="">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Project Description</label>
              <div class="col-md-4">
               <textarea type="text"  class="form-control" placeholder="Enter Project description" name="pdescription" required=""><?php echo $value['project_des']?> </textarea> 
             </div>
           </div>
           <div class="form-group">
            <div class="row">
              <label class="col-md-3 control-label">Project Deadline</label>
              <div class="col-md-2">
                <input type="date" value="<?php echo $value['project_start_date']?>"  class="form-control" placeholder="Start date" name="sdate" required=""> 
              </div>
              <div class="col-md-2">
                <input type="date" value="<?php echo $value['project_end_date']?>"  class="form-control" placeholder="End date" name="edate" required="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Total Budget</label>
            <div class="col-md-4">
              <input type="number" value="<?php echo $value['total_budget']?>" class="form-control" placeholder="Enter totall Budjet of this Project" name="budjet" required="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Budget Mile Stones</label>
            <div class="col-md-4">
              <input type="number" value="<?php echo $value['budget_mile_stone']?>" class="form-control" placeholder="Enter Budjet in Phases" name="budjet_mile" required="">
            </div>
          </div> 
          <div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-offset-3 col-md-4">
                  <button type="submit" class="btn green">Submit</button>
                  <div class="col-md-8"></div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>








