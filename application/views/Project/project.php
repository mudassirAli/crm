<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-10">
        <div class="card">
          <div class="header">
            <h4 class="title">Add Projects</h4>
          </div>
          <hr>
          <div class="content">
            <form class="form-horizontal" action="<?php echo base_url('Project/insert_project');?>" method="POST">
              <div class="form-body">
               <div class="form-group">
                <label class="col-md-3 control-label">Client Name</label>
                <div class="col-md-4">
                  <select name="list" style="width: 99%; height: 30px">
                    <?php
                    foreach ( $key as  $value) 
                      { ?>
                        <option value="<?php echo $value['clients_id']?>"><?php echo $value['clients_name']?></option>
                        <?php }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Project Title</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control" placeholder="Enter Project Name" name="pname" required="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Project Description</label>
                    <div class="col-md-4">
                     <textarea type="text" class="form-control" placeholder="Enter Project description" name="pdescription" required=""></textarea> 
                   </div>
                 </div>
                 <div class="form-group">
                  <div class="row">
                    <label class="col-md-3 control-label">Project Deadline</label>            
                    <div class="col-md-2">
                      <input type="date" class="form-control" placeholder="Start date" name="sdate" required=""> 
                    </div>
                    <div class="col-md-2">
                      <input type="date" class="form-control" placeholder="End date" name="edate" required="">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Total Budget</label>
                  <div class="col-md-4">
                    <input type="number" class="form-control" placeholder="Enter totall Budjet of this Project" name="budjet" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Budget Mile Stones</label>
                  <div class="col-md-4">
                    <input type="number" class="form-control" placeholder="Enter Budjet in Phases" name="budjet_mile" required="">
                  </div>
                </div> 
                <div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green">Submit</button>
                        <div class="col-md-8"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>