 
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/datatables.css');?>">
        <script src="<?php echo base_url('assets/DataTables/datatables.js');?>"></script>
        <script src="<?php echo base_url()?>assets/DataTables/datatables.bootstrap.js"></script>

        <style type="text/css">
        #color
        {
            color: white;
        }
        .alert-success {
            background-color: #337d46!important;
            border-color: #337d46!important;
            color: #27a4b0;
        }
        .modal-backdrop{
            display: none;
        }
        .modal{
            background-color:rgba(0, 0, 0, 0.48);
        }
    </style>  
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                </div>
                                <hr>
                            </div>
                            <?php if ($this->session->flashdata('success')==true) { ?>
                            <div class="alert alert-success" id="color" >
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <?php }?>
                            <table class="table table-striped table-hover table-bordered" id="porject">
                                <thead>
                                    <tr>
                                     <th class="bold">Project name</th>
                                     <th class="bold"> Porject Start </th>
                                     <th class="bold"> Porject End </th>
                                     <th class="bold"> Porject Description </th>
                                     <th class="bold">Task</th>
                                 </tr>
                             </thead>
                         </table>
                     </div>
                 </div>
             </div>

         </div>
     </div>
 </div>

 <div id="myModal" class="modal fade" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Client Feedback</h3>
    </div>
    <div class="modal-body" id="feedback1">
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>




<script type="text/javascript">
    function open_modal(feedback)
    {
        $('#feedback1').html(feedback);
        $('#myModal').modal('show');
    }

</script>



<script type="text/javascript">
    $(document).ready(function() {
        $('#porject').dataTable( {
            processing: true,
            serverSide: true,
            Length:10,
            order: [[ 0, 'desc' ]],
            lengthMenu:[[10,25,50,0],[10,25,50,'All']],
            ajax: {
                url: "<?php echo base_url('Devolper/developersprojects'); ?>",
                method:"GET"
            },

            "columns": [
            {"data" : "project_title"},
            {"data" : "project_start_date"},
            {"data" : "project_end_date"},
            { "data": "project_id",render:function(id, type, row){
               return  '<button type="button"  onclick="open_modal(\''+row.project_des+'\')"  class="btn btn-info">Detail</button>';
           }},
           { "data": "project_id",render:function(id, type, row){
            return  '<a href="<?php echo base_url('Devolper/developertask')?>/'+row.project_id+'"class="btn btn-info">Tasks</a>';
           }},
           ],
           columnDefs:[
           
           {'targets':3,orderable:false},


           ]

       } );
    } );
</script>

