
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add client</h4>
                            </div>
                            <div class="content">
                                <form method="Post" action="<?php echo base_url('Clients/process_add'); ?>" enctype="multipart/form-data" >
                                 <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Client Name</label>
                                                <input type="text" class="form-control" name="clients_name" placeholder="Enter Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="clients_email"  placeholder="Enter Email" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input type="number" class="form-control" name="clients_mob"  placeholder="Enter Password" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Compney Name</label>
                                                <input type="text" class="form-control"  name="clients_compnyName" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" class="form-control" name="clients_address" placeholder="Enter Address">
                                            </div>
                                        </div>
                               
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Project Name</label>
                                                <input type="text" class="form-control" name="clients_projectName" placeholder="Enter Project Name">
                                            </div>
                                        </div>
                                    </div>
                                         <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Project Type</label>
                                                <input type="text" class="form-control" name="clients_projectType"  placeholder="Enter Project Type" >
                                            </div>
                                        </div>
                                    </div>

                                    <input type="submit" class="btn blue" value="Submit">
                                  
                                </form>
                            </div>
                        </div>
                    </div>
         

                </div>
            </div>
        </div>


       