 <h3 class="page-title" style="margin-left:20px">   All Clients View </h3>
 
    <div class="col-md-8 col-sm-12 col-xs-12" style="margin: right">
           
         
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                    
                        <a href="<?php echo base_url('Clients/add'); ?>" style="margin-bottom: 10px;" class="btn green">Add New Purchase</a>
                               

                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr style="background-color: black">

                                    <th> Name</th>
                                    <th>Email</th>
                                    <th>Client Mobile</th>
                                    <th>Client Compony</th>
                                    <th>Client Address</th>
                                    <th>Client Project Name</th>
                                    <th>Client Project Type</th>
                                    <th>Date</th>
                                    <th>Actions</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(isset($clients_view) && !empty($clients_view))
                                {
                                    foreach($clients_view as $puchas)
                                        { ?>
                                            <tr class="odd gradeX" style="background-color: yellow">


                                                <td><?php echo $puchas['clients_name']; ?></td>
                                                <td><?php echo $puchas['clients_email']; ?></td>
                                                <td><?php echo $puchas['clients_mob']; ?></td>
                                                <td><?php echo $puchas['clients_compnyName']; ?></td>
                                          
                                                <td><?php echo $puchas['clients_address'] ?></td>
                                                <td><?php echo $puchas['clients_projectName'] ?></td>
                                                 <td><?php echo $puchas['clients_projectType'] ?></td>
                                                <td><?php echo $puchas['datee'] ?></td>






                                                <td>
                                                    <a href="<?php echo base_url('Team_add/update/'.$puchas['clients_id']); ?>" class="btn blue" type="button" >Update</a>
                                                    <a href="<?php echo base_url('Team_add/delete/'.$puchas['clients_id']); ?>" class="btn blue" type="button" >Delete</a> &nbsp;</td>

                                                </tr>


                                                <?php   
                                            }
                                        }
                                        ?>
                                    </tbody>

                                </table  >
                            </form>
                            <thead>

                                <tr>




                                </tr>
                            </thead>
                        </div>
                    </div>
                </div>
            </div> 
            



