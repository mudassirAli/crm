<!DOCTYPE html>
<html class="no-js" lang="en-GB" prefix="og: http://ogp.me/ns#">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <?php
			$meta_title = "Athlete Event " . $event['organisation'];
			if(isset($page_data['page_name']) && !empty($page_data['page_name']))
			{
				$meta_title .= ' - '.$page_data['page_name'];
			}
			else
			{
				if($page == 'index')
				{
					$meta_title .= ' - Home';
				}
				elseif($page == 'news')
				{
					$meta_title .= ' - News';
				}
				elseif($page == 'news_detail')
				{
					$meta_title .= ' - '.$news['news_headline'];
				}
			}
			if(isset($event['athlete_name']))
			{
				$meta_title .= ' - '.$event['athlete_name'];
			}
			?>
        <title><?php echo $meta_title; ?></title>
        <meta name="description" content="SFS Events - <?php echo "Athlete Event " . $event['organisation']; ?> on <?php echo formated_date ($event['event_date']); ?> at <?php echo $event['event_time']; ?>"/>
        <meta name="keywords" content="SFS Events - <?php echo "Athlete Event " . $event['organisation']; ?><?php echo (isset($event['athlete_name']))?', '.$event['athlete_name']:''; ?>, ">
        <link rel="canonical" href="<?php echo base_url ('event/' . $url . '/'); ?>" />
        <base href="<?php echo base_url (); ?>spm/">
        <meta property="og:locale" content="en_GB" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="SFS Events - <?php echo "Athlete Event " . $event['organisation']; ?> on <?php echo formated_date ($event['event_date']); ?> at <?php echo $event['event_time']; ?>" />
        <meta property="og:description" content="SFS Events - <?php echo "Athlete Event " . $event['organisation']; ?> on <?php echo formated_date ($event['event_date']); ?> at <?php echo $event['event_time']; ?>" />
        <meta property="og:url" content="<?php echo base_url ('event/' . $url . '/'); ?>" />
        <meta property="og:site_name" content="<?php echo "Athlete Event" . $event['organisation']; ?>" />
        <meta property="article:publisher" content="https://www.facebook.com/sportsfschools" />
        <meta property="og:image" content="<?php echo base_url('uploads/event_uploads/event_banners/'.$event['event_banner']); ?>"/>
        <base href="<?php echo base_url (); ?>" />
        <!-- / Yoast WordPress SEO plugin. -->
        <link rel='stylesheet' href='assets/css/main.min.css' type='text/css' media='all' />
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        <script type='text/javascript' src='assets/js/jquery.js'></script>
        <script src="assets/js/sfs_functions.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/admin/bootbox/bootbox.min.js"></script>
        <script>
            var base_url = '<?php echo base_url (); ?>';
            var base_url_admin = '<?php echo base_url ('admin'); ?>';
            //var menu_items = [<?php //echo '"' . implode ('","', $this->config->item ('menu_items')) . '"' ?>];
        </script>
        <div id="fb-root"></div>
		<script>
            window.fbAsyncInit = function() {
                FB.init({
                  appId      : '169739103357380',
                  xfbml      : true,
                  version    : 'v2.4'
                });
              };
        
              (function(d, s, id){
                 var js, fjs = d.getElementsByTagName(s)[0];
                 if (d.getElementById(id)) {return;}
                 js = d.createElement(s); js.id = id;
                 js.src = "https://connect.facebook.net/en_US/sdk.js";
                 fjs.parentNode.insertBefore(js, fjs);
               }(document, 'script', 'facebook-jssdk'));
        </script>
    </head>
    <body class="home page page-template page-template-template-home page-template-template-home-php">

        <header class="banner container" role="banner">
            <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-5"> <a class="brand" href="<?php echo base_url ('event/' . $url . '/'); ?>"><img src="assets/img/sportsforschools.jpg" alt="Sports for Schools Fun Fitness Inspiration"></a> </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <form role="search" method="get" class="search-form form-inline" action="<?php echo base_url ('event/' . $url . '/search/'); ?>">
                        <div class="input-group">
                            <input type="search" autocomplete="off" required placeholder="Find my school" value="" name="s" id="search" class="search-field form-control">
                            <label class="hide">Search for:</label>
                            <span class="input-group-btn">
                                <button type="submit" class="search-submit btn btn-default">Search</button>
                            </span> </div>
                        <div id="search_suggesstions" style="display:none;"></div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="social-strips">
                        <p class="blue-text">Have a laugh at our Facebook page<!--HAVE A LAUGH AT OUR FACEBOOK PAGE--></p>
                        <a href="https://www.facebook.com/sportsfschools" target="_blank"><img src="assets/img/facebook_icon.png" width="120" alt="Facebook"></a> </div>
                    <!--<p class="stay-in-touch">Stay in touch</p>-->
                </div>
                <div class="col-sm-12">
                    <nav class="nav-main" role="navigation">
                        <ul id="menu-primary-navigation" class="nav nav-pills nav-justified">
                            <li class="<?php echo ($page == 'index') ? 'active' : ''; ?> menu-home"><a href="<?php echo base_url ('event/' . $url . '/'); ?>">EVENT HOME</a></li>
                            <?php /*?><li class="<?php echo ($page == 'get-healthy-kids' || $page == 'get-healthy-parents' || $page == 'not-healthy') ? 'active' : ''; ?> dropdown menu-about"><a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">GET HEALTHY <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="<?php echo ($page == 'get-healthy-kids') ? 'active' : ''; ?> menu-whats-it-all-about"><a href="<?php echo base_url ('event/' . $url . '/get-healthy-kids'); ?>">Get Healthy Kids</a></li>
                                    <li class="<?php echo ($page == 'get-healthy-parents') ? 'active' : ''; ?> menu-the-event"><a href="<?php echo base_url ('event/' . $url . '/get-healthy-parents'); ?>">Get Healthy Parents</a></li>
                                    <li class="<?php echo ($page == 'not-healthy') ? 'active' : ''; ?> menu-headteachers"><a href="<?php echo base_url ('event/' . $url . '/not-healthy'); ?>">Not Healthy</a></li>
                                </ul>
                            </li><?php */?>
                            <li class="<?php echo ($page == 'sponsor-a-child') ? 'active' : ''; ?> menu-the-people"><a href="<?php echo (isset ($event['event_sponsor_link']) && ! empty ($event['event_sponsor_link'])) ? $event['event_sponsor_link'] : '#'; ?>" target="_blank" id="spnsr">SPONSOR MY CHILD</a></li>
                            <?php /*?><li class="<?php echo ($page == 'buy-photos') ? 'active' : ''; ?> menu-get-involved"><a href="<?php echo base_url ('event/' . $url . '/buy-photos'); ?>">BUY PHOTOS</a></li><?php */?>
                            <li class="<?php echo ($page == 'about-the-athlete') ? 'active' : ''; ?> menu-contact"><a href="<?php echo base_url ('event/' . $url . '/about-the-athlete'); ?>">About the ATHLETE</a></li>
                           <li class="<?php echo ($page == 'news') ? 'active' : ''; ?> menu-sports-clubs"><a href="<?php echo base_url ('event/' . $url . '/news'); ?>">NEWS</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div class="wrap container" role="document">
            <div class="meet_box btm-pln">
                <div class="heading_container">
                	<h2><span class="step_no">Step 1:</span> <?php echo !empty($generic['step_1_heading'])?$generic['step_1_heading']:'What is it?'; ?></h2>
                </div>
                <div class="meet_athelate_inner">
                  <?php
				  if(!empty($generic['step_1_heading']))
				  {?>
                  	<p><?php echo str_replace(array('{athlete_name}', '{school_name}'), array($event['athlete'], $event['organisation']), $generic['step_1_paragraph']); ?></p>
					<?php	  
				  }
                  else
				  {?>
                 	<p>GB Athlete <?php echo $event['athlete']; ?> is coming to <?php echo $event['organisation']; ?> to lead a fun, inspirational fitness circuit with your child. We're asking you to sponsor your child to raise money for new PE equipment for the school.</p>
                 <?php
				  }?>
				  
                   <button type="button" data-target="<?php echo (isset($_COOKIE['sponsor_popup_for'.$url]) && isset($_SESSION['sponsor_popup_two_for'.$url]))?'#myModal_two':'#myModal';?>" data-toggle="modal" data-dismiss="modal" class="btn btn-primary sponser_box_a sponser_box_ab desktop" id="sponsor-my-child-button"><?php echo !empty($generic['step_1_button_text'])?$generic['step_1_button_text']:'Sponsor My Child'; ?></button>
                </div>
           </div>
            <div class="date_box_n">
                <div class="row">
                    <div class="col-md-3">
                        <div class="icon_event">
                            <img src="assets/img/calender_icon.png" />
                        </div>
                        <div class="icon_event_desc">
                            <h2><?php echo formated_date ($event['event_date']); ?></h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-2">
                        <div class="icon_event">
                            <img src="assets/img/time_icon.png" />
                        </div>
                        <div class="icon_event_desc">
                            <p>Time</p>
                            <h3><?php echo $event['event_time']; ?></h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="icon_event">
                            <img src="assets/img/location_icon.png" />
                        </div>
                        <div class="icon_event_desc">
                            <p>Address</p>
                            <h3><?php echo $event['address_1']; ?></h3>
                            <span><?php echo $event['post_code']; ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="countdown countdown-container">
                            <div class="clock row">
                                <div class="clock-item clock-days countdown-time-value col-sm-6 col-sm-3">
                                    <div class="wrap">
                                        <div class="inner">
                                            <div id="canvas-days" class="clock-canvas"></div>

                                            <div class="text">
                                                <p class="val">0</p>
                                                <p class="type-days type-time">DAYS</p>
                                            </div><!-- /.text -->
                                        </div><!-- /.inner -->
                                    </div><!-- /.wrap -->
                                </div><!-- /.clock-item -->

                                <div class="clock-item clock-hours countdown-time-value col-sm-6 col-md-3">
                                    <div class="wrap">
                                        <div class="inner">
                                            <div id="canvas-hours" class="clock-canvas"></div>

                                            <div class="text">
                                                <p class="val">0</p>
                                                <p class="type-hours type-time">HOURS</p>
                                            </div><!-- /.text -->
                                        </div><!-- /.inner -->
                                    </div><!-- /.wrap -->
                                </div><!-- /.clock-item -->

                                <div class="clock-item clock-minutes countdown-time-value col-sm-6 col-md-3">
                                    <div class="wrap">
                                        <div class="inner">
                                            <div id="canvas-minutes" class="clock-canvas"></div>

                                            <div class="text">
                                                <p class="val">0</p>
                                                <p class="type-minutes type-time">MINUTES</p>
                                            </div><!-- /.text -->
                                        </div><!-- /.inner -->
                                    </div><!-- /.wrap -->
                                </div><!-- /.clock-item -->

                                <div class="clock-item clock-seconds countdown-time-value col-sm-6 col-md-3">
                                    <div class="wrap">
                                        <div class="inner">
                                            <div id="canvas-seconds" class="clock-canvas"></div>

                                            <div class="text">
                                                <p class="val">0</p>
                                                <p class="type-seconds type-time">SECONDS</p>
                                            </div><!-- /.text -->
                                        </div><!-- /.inner -->
                                    </div><!-- /.wrap -->
                                </div><!-- /.clock-item -->
                            </div><!-- /.clock -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            {_yield}
        </div>
        <!-- /.wrap -->
        <?php
        $footer_column_class = 'col-sm-3';
        if (isset ($page_polls) && ! empty ($page_polls))
        {
            $footer_column_class = 'col-sm-2';
        }
        ?>
        <div class="footer_wrap">
            <div class=""><!--container-->
                <div class=""><!--row-->
                    <?php
                    if (isset ($page_polls) && ! empty ($page_polls))
                    {
                        ?>
                        <div class="<?php echo $footer_column_class; ?>">
                            <div class="footer_box">
                                <div id="polls_area" class=""><!--polls area starts--> 
                                    <?php
                                    foreach ($page_polls as $poll_key => $page_poll)
                                    {
                                        ?>
                                        <div class="poll_outer">
                                            <h2><?php echo $page_poll['poll_question']; ?></h2>
                                            <div class="poll_form_<?php echo $page_poll['poll_id']; ?> poll_inner">
                                                <form name="poll_form_<?php echo $page_poll['poll_id']; ?>" id="poll_form_<?php echo $page_poll['poll_id']; ?>" method="post" action="">
                                                    <input type="hidden" name="vote_for_poll" value="<?php echo $page_poll['poll_id']; ?>" />
                                                    <?php
                                                    foreach ($page_poll['poll_options'] as $o_key => $poll_option)
                                                    {
                                                        ?>
                                                        <input type="radio" name="poll_option" class="poll_option" value="<?php echo $poll_option['option_id']; ?>" /> &nbsp; <?php echo $poll_option['option_text']; ?><br />
            <?php }
        ?>
                                                    <input type="button" name="submit_poll_<?php echo $page_poll['poll_id']; ?>" id="submit_poll_<?php echo $page_poll['poll_id']; ?>" class="submit_poll btn btn blue" value="Vote" />
                                                </form>
                                            </div>
                                        </div>
                                        <?php
                                        if (count ($page_polls) > 1)
                                            echo "<br />";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
    <?php
}
?>
                    <div class="<?php echo $footer_column_class; ?>">
                        <div class="footer_box">
                            <h2>Follow On</h2>
                            <div class="social_icon">
                                <ul>
                                    <li><a href="<?php echo (isset ($event['fb_url']) && ! empty ($event['fb_url'])) ? $event['fb_url'] : '#'; ?>" <?php echo (!isset ($event['fb_url']) || empty ($event['fb_url'])) ? 'onclick="return false;"': ''; ?> target="_blank"><img src="assets/img/f_icon.png" alt="Facebook"/></a></li>
                                    <li><a href="<?php echo (isset ($event['twitter_url']) && ! empty ($event['twitter_url'])) ? $event['twitter_url'] : '#'; ?>" <?php echo (!isset ($event['twitter_url']) || empty ($event['twitter_url'])) ? 'onclick="return false;"': ''; ?>  target="_blank"><img src="assets/img/t_icon.png" alt="Twitter"/></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="<?php echo $footer_column_class; ?>">
                        <div class="footer_box">
                            <h2>Fun Kids</h2>
                            <div class="footer_logo">
                                <a href="http://www.funkidslive.com/ " target="_blank"><img src="assets/img/fk.png" alt="Fun Kids Digital Radio and Online"/></a>
                            </div>
                        </div>
                    </div>
                    <div class="<?php echo $footer_column_class; ?>">
                        <div class="footer_box">
                            <h2>Fundraising Standards Board</h2>
                            <div class="footer_logo">
                                <a href="http://www.sportsforschools.org/membership/" target="_blank"><img src="<?php echo 'uploads/event_generic_uploads/' . $generic['fsb_logo']; ?>" alt="Fund Raising Standard Board"/></a>
                            </div>
                        </div>
                    </div>
                    <div class="<?php echo $footer_column_class; ?>">
                        <div class="footer_box">
                            <h2>We are a Social Enterprise</h2>
                            <div class="footer_logo">
                                <a href="http://www.sportsforschools.org/membership/" target="_blank"><img src="<?php echo 'uploads/event_generic_uploads/' . $generic['se_logo']; ?>" alt="Socieal Enterprize"/></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="copyright_wrap">
            <div class=""><!--container-->
                <footer class="content-info" role="contentinfo">
                    <div class=""><!--row-->
                        <div class="col-sm-4">
                            <p>&copy; 2015 Sports for Schools </p>
                        </div>
                        <div class="col-sm-4">
							<p class="text-center"><a href="<?php echo base_url ('event/' . $url . '/privacy-policy'); ?>">Privacy Policy</a></p><?php  ?>
                        </div>
                        <div class="col-sm-4">
                            <p class="text-right">Event By <a href="http://sportsforschools.org/" target="_blank">Sportsforschools.org</a></p>
                        </div>
                    </div>
                </footer>
            </div>
            <div class="clearfix"></div>
        </div>
        
<?php if (!isset($_COOKIE['sponsor_popup_for'.$url]) ) {  ?>
<button style="display:none;" type="button" class="btn btn-primary btn-lg" data-toggle="modal" id="saqib" data-target="#myModal">
  Launch demo modal
</button>
<?php }
    setcookie("sponsor_popup_for".$url, 'yes', time()+86400);
?>
<!-- Modal -->
<div class="modal fade sponsor_popup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form name="form1" id="form1" action="<?php echo site_url ('register/sponsor_childcontact');?>" method="POST" novalidate onSubmit="return false;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo !empty($generic['popup_heading'])?$generic['popup_heading']:'Sponsor My Child'?></h4>
      </div>
      <div class="modal-body">
       <div class="sponser_boxpopop">
       		<div class="error" id="sponsor-form-error"></div>
         <div class="sponser_form">
           <ul>
             <li><label>Child  First Name</label><input name="first_name" id="first_name" value="" required type="text" /></li>
             <li><label>School Year</label><input name="school_year" value="<?php echo isset($_SESSION['school_year'.$event['event_url']])?$_SESSION['school_year'.$event['event_url']]:'';?>" required type="text" /></li>
             <li><label>Your Name</label><input name="your_name" value="<?php echo isset($_SESSION['your_name'.$event['event_url']])?$_SESSION['your_name'.$event['event_url']]:'';?>" required type="text" /></li>
             <li><label>Your E-mail Address</label><input name="email" value="<?php echo isset($_SESSION['email'.$event['event_url']])?$_SESSION['email'.$event['event_url']]:'';?>" required type="email" />
                 <input name="url" value="<?php echo $url;?>" required type="hidden" />
                 <input name="donate_url" value="<?php echo $event['event_sponsor_link'];?>" required type="hidden" />
                 <input name="event_url" value="<?php echo $event['event_url'];?>" type="hidden" />
                 <input name="school_name" value="<?php echo $event['organisation'];?>" type="hidden" />
                 <input name="athlete_is_tag_id" value="<?php echo $event['infusionsoft_tag_id'];?>" type="hidden" />
             </li>
             <li style="width:100%;"><label style=" font-size:13px !important;" for="optin"><input type="checkbox" name="optin" id="optin" value="Yes" <?php echo ((isset($_SESSION['optin'.$event['event_url']]) && $_SESSION['optin'.$event['event_url']] == 'Yes') || !isset($_SESSION['optin'.$event['event_url']]))?'checked="checked"':'';?> />
			 	<?php 
				if(!empty($generic['popup_checkbox_text']))
				{
					echo $generic['popup_checkbox_text'];
				}
				else
				{ ?>
					I am happy to hear from Sports for Schools from time to time with updates and interesting information
				<?php
				}?>
       			</label>
           	</li>
           </ul>
         </div>
        </div>
     </div>
      <div class="modal-footer">
        <p><?php 
		if(!empty($generic['popup_form_text']))
		{
			echo $generic['popup_form_text'];
		}
		else
		{ ?>
        	When you click sponsor, you’ll be taken through to our partners at Make-a-Donation. You can set up your child’s fundraising page in just a few clicks.
        <?php
		}?>
        </p>
        <button type="submit" class="btn btn-primary sponser_box_a">Get Fundraising</button>
      </div>
            </form>
    </div>
  </div>
</div> 
<div class="modal fade sponsor_popup" id="myModal_two" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form name="form1" id="form1" action="<?php echo site_url ('register/sponsor_childcontact');?>" method="POST" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo !empty($generic['popup_heading'])?$generic['popup_heading']:'Sponsor My Child'?></h4>
      </div>
      <div class="modal-body">
       <div class="sponser_boxpopop">
         <div class="sponser_form">
             <span style="color: green;" id="sponsor-success-msg"><?php echo $this->session->flashdata('successMessage'); ?></span>
             <div class="share-icon">
             	<span class='st_facebook_large' displayText='Facebook'></span>
                <span class='st_twitter_large' displayText='Tweet'></span>
                <span class='m-a-d'><a href="https://make-a-donation.org/" title="make-a-donation" target="_blank"><img src="assets/img/m-a-d.png" height="32" width="32" alt="make-a-donation" /></a></span>
             </div>
             <div style="text-align:justify !important;" id="re-sponsor-text">
                <?php if(!empty($generic['popup_long_text']))
				{
					echo $generic['popup_long_text'];
				}
				else
				{ ?>
                	<p>Looks like you've already filled in the form. If that's not the case, or you'd like fill it out for another child, just click on the button below."
We'd love to see you on Facebook or Twitter – we generally post funny stuff and interesting on Facebook, and use Twitter to chat to schools, athletes and anyone else who wants to join the conversation.</p>
				<p>If you've already set up a Fundraiser page with our friends at Make-a-Donation, you'll have received an email link from them. Or go click on the logo here, and search for your fundraiser page.</p>
                <?php
				}?>
		</div>
         </div>
        </div>
     </div>
      <div class="modal-footer">
          <button type="button" onclick="$('#myModal_two').modal('hide');$('#myModal').modal('show');" class="btn btn-primary sponser_box_a"><?php echo !empty($generic['popup_button_text'])?$generic['popup_button_text']:'Sponsor another child'?></button>
      </div>
            </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "c277f61f-5cd6-4f1d-83f2-9eb430afeedb", doNotHash: true, doNotCopy: false, hashAddressBar: false});</script>
<script src="assets/admin/jquery-validation/dist/jquery.validate.js" type="text/javascript" ></script>
<script type="text/javascript" src="assets/js/kinetic.js"></script>
<script type="text/javascript" src="assets/js/jquery.final-countdown.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.js"></script>
        <script type="text/javascript">  
            $('document').ready(function() {
                'use strict';
        
                $('.countdown').final_countdown({
                    'start': '<?php echo time (); ?>',
                    'end': '<?php echo strtotime ($event['event_date']) ?>',
                    'now': '<?php echo time (); ?>'        
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#search").keyup(function() {
                    var search_input = $('#search').val();
                    if( search_input == "" )
                    {
                        $("#search_suggesstions").html("");
						$("#search_suggesstions").hide();
                    }
                    else
                    {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url ().'spm/front/search_suggestions/'; ?>",
                            data: "search_input="+ search_input,
                            dataType: 'json',
                            success: function(data){
                                var suggestions = remove_spaces_from_html(data.html);
                                $("#search_suggesstions").html(suggestions).show();
                            }
                        });
                    }
                });
				
				
				$('#form1').validate({
					submitHandler: function(form) {
						window.open('<?php echo $event['event_sponsor_link'];?>', '_blank');
						$.ajax({
							type: 'POST',
							url: "<?php echo base_url ().'register/sponsor_childcontact'; ?>",
							data: $("#form1").serialize(),
							cache: false,
							dataType: 'json',
							success: function(response){
								if(response.result == 'error')
								{
									$('#sponsor-form-error').text(response.message);
								}
								else
								{
									$('#sponsor-my-child-button').attr('data-target', '#myModal_two');
									$('#myModal').modal('hide');
									$('#sponsor-success-msg').text(response.message);
									$('#myModal_two').modal('show');
									$('#re-sponsor-text').hide();
									$('#first_name').val('');
									setTimeout(function(){
										$('#sponsor-success-msg').html('')
										$('#re-sponsor-text').show();
										}, 5000);
								}
							}
						});	
					}
				});
            });
        </script>
    </body>
</html>