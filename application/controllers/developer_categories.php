<?php


class Developer_categories extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/dashboard';
		$this->load->model('Developer_categorie_model','categories');
	}
	public function index()
	{
		
		$data['categories_view'] = $this->categories->get_all();
		$this->load->view('developer_categories/index',$data);	
	}
		public function add()
	{
		$this->load->view('developer_categories/add');	
	}

	public function process_add()
	{

			$this->form_validation->set_rules('devlprtype_name',' Name','required|is_unique[devlprtype.devlprtype_name]');
			// $this->form_validation->set_rules('devlprtype_datee','Email','required');
		
			if ($this->form_validation->run()==false) 
			{
				
				$this->load->view('developer_categories/add');	
			}
			else
			{
	           	$developer_type = $this->input->post('devlprtype_name');
				// $developer_date = $this->input->post('devlprtype_datee');

				
				$data = 
				array( 
						'devlprtype_name' => $developer_type
						// 'devlprtype_datee' => $developer_date
				);
	             	$this->categories->save($data);
	             	$this->success();
	             	redirect('Developer_categories/index');
    	}
	}

	   public function delete($category_id)
    {
        if (isset($category_id) && !empty($category_id)) {
            $this->categories->delete_by('devlprtype_id', $category_id);
            $this->session->set_flashdata('success_message', 'categories has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete categories.');
        }
        redirect('Developer_categories/index');
    }

     public function update($category_id)
    {
        $data = array();
   
        if (isset($category_id) && !empty($category_id)) {
            $data['data'] = $this->categories->get_by('devlprtype_id', $category_id);
            if (isset($data['data']) && !empty($data['data'])) 
            {   
                $this->load->view('developer_categories/update',$data);
            }
            else
            {
                $this->session->set_flashdata('error_message', 'Product not found.');
            }
        }
     
    }
    public function success()
	{
		$this->session->set_flashdata('success', 'Successfully Record Inserted');
		redirect('Developer_categories/index');
	}
    public function process_update()
    {
		$id = $this->input->post('devlprtype_id');
		$developer_type = $this->input->post('devlprtype_name');
		// $developer_date = $this->input->post('devlprtype_datee');
		$data = array( 
						'devlprtype_name' => $developer_type
		);
		$this->categories->update_by('devlprtype_id',$id,$data);
 		redirect('Developer_categories/index');

	}
     

}
?>