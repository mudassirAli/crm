<?php


class Devolper extends CI_Controller {

	public function __construct()
	{
		 parent::__construct();
		  $this->layout = 'admin/dashboard';
		  $this->load->model('Devolper_model','d');
	}
	public function index()
	{
		
		$data['devolper_view'] = $this->d->get_all();
		$this->load->view('devolper/index',$data);
		
	}
	public function developerproject()
	{
		$this->load->view('devolper/showdeveloperproject');
	}
	public function developertask($projectid)
	{
		if($this->session->userdata('user_email'))
        {
		     if($this->session->userdata('role')=='developer')
		     {
		     	 $id=$this->session->userdata('id');

		         $data['developertasks']=$this->Devolper_model->show_developer_task($id,$projectid);
				 $this->load->view('devolper/developertask',$data);
		  
		     // elseif ($this->session->userdata('role')=='admin') 
		     //  {
		     //  	//show all developers projects;
		     //  }
	}
}
                   else
		            {
		            	
		        		redirect('LoginCI/login');
		            }
	}
		public function developersprojects()
	{ 
        if($this->session->userdata('user_email'))
        {
		     if($this->session->userdata('role')=='developer')
		     {
		     	
		$id=$this->session->userdata('id');
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start+1:$start;

		if($length)
		$this->db->limit($length);
		$this->db->offset($start);
		$value = '';
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
		}

		if(isset($order[0]['column']))
		{
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);
			$show_table = $this->Devolper_model->show_developer_project($id,$value);
			$count_data =count($this->Devolper_model->show_developer_project($id,$value));
			//$count_data = $this->Devolper_model->count_data($id);
			$response['draw']= $draw;
			$response['recordsTotal']= $count_data;
			$response['recordsFiltered'] = $count_data;
			$response['data']=$show_table;
			echo json_encode($response);
			exit;
	}
}
}
                   else
		            {
		            	
		        		redirect('LoginCI/login');
		            }
}
		  //   	$data['developerprojects']=$this->Devolper_model->show_developer_project($id);
				// $this->load->view('devolper/showdeveloperproject',$data);
		  
		     // elseif ($this->session->userdata('role')=='admin') 
		     // {
		     // 	//show all developers projects;
		     // }
		     

		          

	 	public function donetask($id)
	 {
		
	 	$data = array('status' =>1,);
	 	$this->Devolper_model->updateto('tasks',$data,$id,'tasks_id');
	 	$data2 = array('status' =>1,);
	 	$this->Devolper_model->updateto('asign_to',$data,$id,'taskid');
	 	redirect('Devolper/developertask/'.$id);

		
	 }

	public function process_add()
	{
		$data = array();
		if ($this->input->post()) {

			
		
			
		
			if ($data = $this->input->post()){
				$data = $this->input->post();
				$data['devlprtype_datee'] = date('Y-m-d');
		
	           
	              if($category_id = $this->d->save($data)) {

	              	$this->session->set_flashdata('success_message', 'Category has been saved successfully');

	              	redirect('Devolper/');
	              } else {
	              	$this->session->set_flashdata('error_message', 'Error occured while saving category.');
	              	redirect('Devolper/');
	              }
	          }else{

	          	$this->load->view('devolper/add');
	          }
	      } else {
	      	$this->session->set_flashdata('error_message', 'Error occured while saving category.');
	      	redirect('Devolper');;
	      }
	  }

	   public function delete($devlprtype_id)
    {
        if (isset($devlprtype_id) && !empty($devlprtype_id)) {
            $this->d->delete_by('devlprtype_id', $devlprtype_id);
            $this->session->set_flashdata('success_message', 'product has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
        }
        redirect('Team_add/index');
    }

     public function update($team_id)
    {
        $data = array();
   
        if (isset($team_id) && !empty($team_id)) {
            $data['data'] = $this->team->get_by('team_id', $team_id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                // $data['categories'] = $this->categories->get_all();
                //  $data['all_categories'] = $this->sub_categories->get_by('team_id',$team_id);
                // $data['sub'] = $this->sub_categories->get_all();
                $this->load->view('team/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Product not found.');
                redirect('Team_add/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('Team_add/',$data);
        }
    }

     public function process_update()
    {
        $data = array();
        if ($this->input->post('team_id')) {

            $team_id = $this->input->post('team_id');
			$this->form_validation->set_rules('team_name',' Name','required');
			$this->form_validation->set_rules('team_email','Email','required');
			$this->form_validation->set_rules('team_password','Password','required');
			// $this->form_validation->set_rules('team_img','Image','required');
			$this->form_validation->set_rules('team_desgnation','Designation','required');
            if ($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
                //debug($data,true);
                if(isset($_FILES['team_img']['tmp_name']) && !empty($_FILES['team_img']['tmp_name']))
				{
					$config['upload_path']   = BASEPATH.'../uploads/'; 
					echo $config['upload_path']; 
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
					$config['max_size']      = 10000; 
					$config['max_width']     = 4000; 
					$config['max_height']    = 4000;  
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('team_img')){
						$error = array('error' => $this->upload->display_errors());
						// debug($error,true);
					}
					else{
	                      $uploaded_image = $this->upload->data(); // end image upload
	                      $data['product_image'] = $uploaded_image['file_name'];
	                  }
	            }

                if ($this->team->update_by('team_id', $team_id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('Team_add/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('Team_add/');
                }
            }
            else{
          //   	$data['data'] = $this->product->get_by('product_id', $product_id);	
          //   	 $data['categories'] = $this->categories->get_all();
	         //  	$data['sub'] = $this->sub_categories->get_all();
          //   	$data['all_catagory'] = $this->product->get_by('product_id', $product_id);
		        // $data['data'] = $data['data'][0];
		      
		        $this->load->view('team/update');
            }
            
        }
        
    }

}
?>