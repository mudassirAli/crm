<?php


class Team_add extends CI_Controller {

	public function __construct()
	{
		 parent::__construct();
		  $this->layout = 'admin/dashboard';
		  $this->load->model('team_model','team');
		  $this->load->model('Devolper_model','p');
	}
	public function index()
	{
		$data['team_view'] = $this->team->get_all();
		$this->load->view('team/index',$data);	
	}

	public function add()
	{
		$data['projects'] = $this->p->get_all();
		$this->load->view('team/add',$data);
		
	}

	public function process_add()
	{
		$data = array();
		if ($this->input->post()) {

			
			$this->form_validation->set_rules('team_name',' Name','required');
			$this->form_validation->set_rules('team_email','Email','required');
			$this->form_validation->set_rules('team_password','Password','required');
			// $this->form_validation->set_rules('team_img','Image','required');
			$this->form_validation->set_rules('team_desgnation','Designation','required');
		
			if ($this->form_validation->run() === TRUE ){
				$data = $this->input->post();
				$data['datee'] = date('Y-m-d');
				 
				if(isset($_FILES['team_img']['tmp_name']))
				{

					$config['upload_path']   = BASEPATH.'../uploads/'; 
					 
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
					// debug($config['allowed_types'],true);
					$config['max_size']      = 10000; 
					$config['max_width']     = 4000; 
					$config['max_height']    = 4000;  
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('team_img')){
						$error = array('error' => $this->upload->display_errors());
						 debug($error,true);
					}
					else{
	                      $uploaded_image = $this->upload->data(); // end image upload
	                      //debug($uploaded_image,true);
	                      $data['team_img'] = $uploaded_image['file_name'];
	                  }
	            }
	            if(isset($data['submit'])){
	            	unset($data['submit']);
	            }
	           
	              if($category_id = $this->team->save($data)) {

	              	$this->session->set_flashdata('success_message', 'Category has been saved successfully');

	              	redirect('Team_add');
	              } else {
	              	$this->session->set_flashdata('error_message', 'Error occured while saving category.');
	              	redirect('Team_add');
	              }
	          }else{

	          	$this->load->view('team/add');
	          }
	      } else {
	      	$this->session->set_flashdata('error_message', 'Error occured while saving category.');
	      	redirect('Team_add');;
	      }
	  }

	   public function delete($team_id)
    {
        if (isset($team_id) && !empty($team_id)) {
            $this->team->delete_by('team_id', $team_id);
            $this->session->set_flashdata('success_message', 'product has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
        }
        redirect('Team_add/index');
    }

     public function update($team_id)
    {
        $data = array();
   
        if (isset($team_id) && !empty($team_id)) {
            $data['data'] = $this->team->get_by('team_id', $team_id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                // $data['categories'] = $this->categories->get_all();
                //  $data['all_categories'] = $this->sub_categories->get_by('team_id',$team_id);
                // $data['sub'] = $this->sub_categories->get_all();
                $this->load->view('team/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Product not found.');
                redirect('Team_add/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('Team_add/',$data);
        }
    }

     public function process_update()
    {
        $data = array();
        if ($this->input->post('team_id')) {

       

            $team_id = $this->input->post('team_id');
           
				$this->form_validation->set_rules('team_name',' Name','required');
			$this->form_validation->set_rules('team_email','Email','required');
			$this->form_validation->set_rules('team_password','Password','required');
			// $this->form_validation->set_rules('team_img','Image','required');
			$this->form_validation->set_rules('team_desgnation','Designation','required');
            if ($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
                //debug($data,true);
                if(isset($_FILES['team_img']['tmp_name']) && !empty($_FILES['team_img']['tmp_name']))
				{
					$config['upload_path']   = BASEPATH.'../uploads/'; 
					echo $config['upload_path']; 
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
					$config['max_size']      = 10000; 
					$config['max_width']     = 4000; 
					$config['max_height']    = 4000;  
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('team_img')){
						$error = array('error' => $this->upload->display_errors());
						// debug($error,true);
					}
					else{
	                      $uploaded_image = $this->upload->data(); // end image upload
	                      $data['product_image'] = $uploaded_image['file_name'];
	                  }
	            }

                if ($this->team->update_by('team_id', $team_id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('Team_add/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('Team_add/');
                }
            }
            else{
          //   	$data['data'] = $this->product->get_by('product_id', $product_id);	
          //   	 $data['categories'] = $this->categories->get_all();
	         //  	$data['sub'] = $this->sub_categories->get_all();
          //   	$data['all_catagory'] = $this->product->get_by('product_id', $product_id);
		        // $data['data'] = $data['data'][0];
		      
		        $this->load->view('team/update');
            }
            
        }
        
    }

}