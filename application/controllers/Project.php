<?php   
class Project extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/dashboard';
		$this->load->model('Project_details_model');
	}
	public function project_add()
	{
		$client['key'] = $this->Project_details_model->select_client();
		$this->load->view('Project/project',$client);
	}
	public function insert_project()
	{
		$project_details = array(
			'client_id' => $this->input->post('list'),
			'project_title' => $this->input->post('pname'),
			'project_des' => $this->input->post('pdescription'),
			'project_start_date' => $this->input->post('sdate'),
			'project_end_date' => $this->input->post('edate'),
			'total_budget' => $this->input->post('budjet'),
			'budget_mile_stone' => $this->input->post('budjet_mile')

		);
		$this->Project_details_model->save($project_details);
 	    $this->success_add();
	}
	public function allProject()
	{
		$this->load->view('project/allProject');
	}
	public function select_project()
	{
		$client['key'] = $this->Project_details_model->select();
		$this->load->view('project/allProject');
	}
	public function datatable()
	{
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start:$start;

		if($length)
			$this->db->limit($length);
		$this->db->offset($start);
		$value = '';
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
		}

		if(isset($order[0]['column']))
		{
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);

			$show_table = $this->Project_details_model->show_project_datatble($value);

			//$count_data = $this->Project_details_model->count_data();
			$count_data= count($this->Project_details_model->show_project_datatble($value));

			$response['draw']= $draw;
			$response['recordsTotal']= $count_data;
			$response['recordsFiltered'] = $count_data;
			$response['data']=$show_table;
			echo json_encode($response);
			exit;
		}
	}
	public function delete_project($id)
	{
		$this->Project_details_model->delete_client('project_id',$id);
		$this->success_delete();
	}
	public function edit_project($id)
	{
		$data['key'] = $this->Project_details_model->select_edit($id);
		$this->load->view('Project/update_project',$data);
	}

	public function update()
	{	
		$id = $this->input->post('project_id');
		$data = array( 
			'project_title' => $this->input->post('pname'),
			'project_des' => $this->input->post('pdescription'),
			'project_start_date' => $this->input->post('sdate'),
			'project_end_date' => $this->input->post('edate'),
			'total_budget' => $this->input->post('budjet'),
			'budget_mile_stone' => $this->input->post('budjet_mile')
		);
		$this->Project_details_model->update_project_details($id,$data);
		$this->success_update();
	}
	public function success_add()
	{
		$this->session->set_flashdata('success', 'Successfully Project Inserted');
		redirect('Project/allProject');
	}
	public function success_update()
	{
		$this->session->set_flashdata('success', 'Successfully Record updated');
		redirect('Project/allProject');
	}
	public function success_delete()
	{
		$this->session->set_flashdata('success', 'Successfully Record deleted');
		redirect('Project/allProject');
	
	}







}
?>