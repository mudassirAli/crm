<?php


class AsignTask extends CI_Controller {

	public function __construct()
	{
		 parent::__construct();
		  $this->layout = 'admin/dashboard';
		  $this->load->model('AsignTask_Model','a');
		  $this->load->model('Devolper_model','p');
	}
	public function index()
	{
		
		$data['taskview'] = $this->a->get_alll();
		// debug($data['taskview'],true);
		$this->load->view('AsignTask/index',$data);
		
	}
		public function add()
	{
		$data['projects'] = $this->p->get_all();
		$this->load->view('AsignTask/add',$data);
		
	}

	public function ajex_add()
	{
	
		$devlprtype=$this->input->post('devlprtype');

		$data = $this->a->get_dropdown($devlprtype);
		
		echo json_encode($data);
		exit;
	
	}

	public function process_add()
	{
		
		$data = array('asign_to_tital' => $this->input->post('asign_to_tital'),'asign_to_Description' => $this->input->post('asign_to_Description'),'devlprtype_id' => $this->input->post('devlprtype_id'),'asign_to_name'=>$this->input->post('asign_to_name'),'asign_to_DeadlineTo'=>$this->input->post('asign_to_DeadlineTo'));
		// print_r($data); exit();
		if ($this->input->post()) {
			$table1_data = array();
			$table1_data['asign_to_tital'] = $data['asign_to_tital'];
			$table1_data['asign_to_Description'] = $data['asign_to_Description'];
			$table1_data['devlprtype_id'] = $data['devlprtype_id'];
			$table1_data['asign_to_name'] = $data['asign_to_name'];
			$table1_data['asign_to_DeadlineTo'] = $data['asign_to_DeadlineTo'];
			$table2_data = array();


			
				// $data = $this->input->post();
				// $data['datee'] = date('Y-m-d');
			
	           
	             $tasks_asign_id = $this->a->save($data);

                 $tasks_detaile = $_POST['tasks_detaile'];
                 // $tasks_name = $_POST['tasks_name'];
                 // debug( $tasks_name ,true);
                 $tasks_datefrom = $_POST['tasks_datefrom'];
                 $tasks_datetoo = $_POST['tasks_datetoo'];
                 $tasks_time = $_POST['tasks_time'];

               foreach ($_POST['tasks_detaile'] as $index =>$value) {
					$tmp_array = array();
					$tmp_array['taskss_id'] = $tasks_asign_id;
					$tmp_array['tasks_name'] = isset($_POST['tasks_name'][$index])?$_POST['tasks_name'][$index]:'';
					$tmp_array['tasks_detaile'] = $value;
					$tmp_array['tasks_datefrom'] = isset($tasks_datefrom)?$tasks_datefrom[$index]:'';
					$tmp_array['tasks_datetoo'] = isset($tasks_datetoo)?$tasks_datetoo[$index]:'';
					$tmp_array['tasks_time'] = isset($tasks_time)?$tasks_time[$index]:'';
					$table2_data[] = $tmp_array;
			    }
			    // debug($table2_data,true);
				$neww=$this->db->insert_batch('tasks', $table2_data); 
					



	              	$this->session->set_flashdata('success_message', 'Category has been saved successfully');

	              	redirect('AsignTask/');
	               
	         }
	     }
	  
	  

	   public function delete($asign_to_id)
    {
        if (isset($asign_to_id) && !empty($asign_to_id)) {
            $this->a->deleted($asign_to_id);
            $this->session->set_flashdata('success_message', 'product has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
        }
        redirect('AsignTask/index');
    }

     public function update($team_id)
    {
        $data = array();
   
        if (isset($team_id) && !empty($team_id)) {
            $data['data'] = $this->team->get_by('team_id', $team_id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                // $data['categories'] = $this->categories->get_all();
                //  $data['all_categories'] = $this->sub_categories->get_by('team_id',$team_id);
                // $data['sub'] = $this->sub_categories->get_all();
                $this->load->view('team/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Product not found.');
                redirect('Team_add/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('Team_add/',$data);
        }
    }

     public function process_update()
    {
        $data = array();
        if ($this->input->post('team_id')) {

       

            $team_id = $this->input->post('team_id');
           
				$this->form_validation->set_rules('team_name',' Name','required');
			$this->form_validation->set_rules('team_email','Email','required');
			$this->form_validation->set_rules('team_password','Password','required');
			// $this->form_validation->set_rules('team_img','Image','required');
			$this->form_validation->set_rules('team_desgnation','Designation','required');
            if ($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
                //debug($data,true);
                if(isset($_FILES['team_img']['tmp_name']) && !empty($_FILES['team_img']['tmp_name']))
				{
					$config['upload_path']   = BASEPATH.'../uploads/'; 
					echo $config['upload_path']; 
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
					$config['max_size']      = 10000; 
					$config['max_width']     = 4000; 
					$config['max_height']    = 4000;  
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('team_img')){
						$error = array('error' => $this->upload->display_errors());
						// debug($error,true);
					}
					else{
	                      $uploaded_image = $this->upload->data(); // end image upload
	                      $data['product_image'] = $uploaded_image['file_name'];
	                  }
	            }

                if ($this->team->update_by('team_id', $team_id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('Team_add/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('Team_add/');
                }
            }
            else{
          //   	$data['data'] = $this->product->get_by('product_id', $product_id);	
          //   	 $data['categories'] = $this->categories->get_all();
	         //  	$data['sub'] = $this->sub_categories->get_all();
          //   	$data['all_catagory'] = $this->product->get_by('product_id', $product_id);
		        // $data['data'] = $data['data'][0];
		      
		        $this->load->view('team/update');
            }
            
        }
        
    }

}
?>