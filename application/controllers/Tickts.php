<?php
class Tickts extends CI_Controller
{
	
	public function __construct()
	{
		 parent::__construct();
		 $this->layout = 'admin/dashboard';
		 $this->load->model('Tickts_model');
		 $this->load->helper('custom_helper');

	}
	public function index()
	{ 
		$data['taskdat']=$this->Tickts_model->getdata();
		$this->load->view('Task/showtask',$data);
	}
	public function showtask()
	{
		$data['taskdat']=$this->Tickts_model->getdata();
		$this->load->view('Task/showtask',$data);
	}
	public function showticktform($id)
	{
		 $data = array();
		 $data['ticket_id'] = $id;
		 $data['pname']=$this->input->post('pname');
         $data['taskdata']=$this->Tickts_model->showtickts($id);
		 $this->load->view('Tickts/showickts',$data);
		
	}
	public function addtickt()
	{
		$id=$this->input->post('id');
		$data = array(
			'task_id' =>$this->input->post('id'),
			'tickt_desc' =>$this->input->post('ticktdesc'),
			'tickt_date' =>$this->input->post('date')
		);
		$this->Tickts_model->insert($data);
		redirect('Tickts/showticktform/'.$id);
	}
	public function pak($id)
	{
		$idd['id']=$id;
		$this->load->view('Tickts/addtickt',$idd);
	}
	public function practic()
	{
		 $data['ticket_id'] =2;
         $data['taskdata']=$this->Tickts_model->show(10);
		 $this->load->view('Tickts/nulltickts',$data);
		
	}
}

?>