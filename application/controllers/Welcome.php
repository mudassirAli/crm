<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		 parent::__construct();
		  $this->layout = 'admin/dashboard';
		  $this->load->model('Welcome_model');
	}
	public function add()
	{
		
		$data['show_country'] = $this->Welcome_model->get_all();
		$this->load->view('welcome_add',$data);
	}
	public function index()
	{
		$this->load->view('welcome_message');
	}
}
