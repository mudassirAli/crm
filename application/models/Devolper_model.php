<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('Abstract_model.php');

class Devolper_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "devlprtype";
		    parent::__construct();
    }

    
    public function show_developer_project($id,$value)
  {
       $this->db->select('*');
       $this->db->from('asign_to');
       $this->db->join('projects','asign_to.project_id=projects.project_id');
       $this->db->where('asign_to.team_id',$id);
    if(!empty($value))
      {
        $this->db->where("(
          projects.project_title like '%$value%' OR
          projects.project_start_date like '%$value%' OR
          projects.project_end_date like '%$value%' 
          )");
      }
    $query = $this->db->get();
    return $query->result_array(); 
  }

   public function show_developer_task($id,$projectid)
  {
       $this->db->select('*');
       $this->db->from('asign_to');
       $this->db->join('projects','asign_to.project_id=projects.project_id');
       $this->db->where('asign_to.team_id',$id);
       $this->db->where('asign_to.project_id',$projectid);
       $this->db->where('asign_to.status',0);
       $query = $this->db->get();
       return $query->result_array(); 
    // return $this->db->last_query(); 
  }

  public function count_data($id)
  {
        $this->db->select('*');
        $this->db->from('asign_to');
        $this->db->join('projects','asign_to.project_id=projects.project_id');
        $this->db->where('asign_to.team_id',$id);
        return $this->db->count_all_results();
  }
   public function updateto($table,$data,$id,$coloum)

  {
         $this->db->where($coloum,$id);
         $this->db->update($table, $data);
  }
   	
    // public function get_tasks($project_id)
    // {
    // 	$this->db->select('*');
    // 	$this->db->from('asign_to');
    // 	$this->db->where('asign_to.project_id',$project_id);
    // 	$this->db->join('tasks','tasks.project_id = asign_to.project_id');
    // 	echo $this->db->last_quer
    // 	$data=$this->db->get();
    // 	return $data->result_array();
    // }



	

}