<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('Abstract_model.php');

class AsignTask_Model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "asign_to";
        $this->table_namee = "tasks";
		parent::__construct();
    }

    function get_dropdown($devlprtype)
	{
		
		return $this->db->select("*")
		->from("tem_add")
		// ->join('categories',"categories.$brand_id = sub_categories.sub_category_category_id")
		 ->where('devlprtype_id',$devlprtype)
		->get()->result_array();
	}

	public function get_alll() 
	{
	$this->db->select('*');  
	$this->db->from('asign_to');
	$this->db->join('tasks','tasks.taskss_id = asign_to.asign_to_id');
	$this->db->join('devlprtype','devlprtype.devlprtype_id = asign_to.devlprtype_id');
	$this->db->join('tem_add','tem_add.team_id = asign_to.asign_to_name');  
	// $this->db->join('supliers','supliers.Supliers_id = purchases.purchase_suplier_id');
	$data = $this->db->get();
	return $data->result_array();
	}
     public function deleted($asign_to_id)
	

	{
         $this->db->delete($this->table_name, array('asign_to_id' => $asign_to_id));
         $this->db->delete($this->table_namee, array('taskss_id' => $asign_to_id));

    }
       
		
    


	

}