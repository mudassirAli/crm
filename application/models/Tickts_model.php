<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('Abstract_model.php');

class Tickts_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "tickts";
		parent::__construct();
    }
    public function getdata()
    {
    return $this->db->select()->from('tasks')->join('projects','projects.project_id=tasks.project_id')->get()->result_array();
    }
    public function insert($data)
    {
    	$this->db->insert($this->table_name,$data);
    	return true;
    }
     public function showtickts($id)
    {
    return $this->db->select()->from($this->table_name)->where('task_id',$id)->get()->result_array();
    }

    public function show($id)
    {
       $this->db->select('*');
       $this->db->from('asign_to');
       $this->db->join('projects','asign_to.project_id=projects.project_id');
       $this->db->where('asign_to.team_id',$id);
       $data=$this->db->get();
       return $data->result_array();
    }
    


	

}