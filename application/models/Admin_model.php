
<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Admin_model extends Abstract_model {

    protected $table_name = "";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "users";
		parent::__construct();
    }
 
	public function client_login($mail, $password,$role)
	{
		$this->db->select();
		$this->db->from('clients');
		$this->db->where('clients_email',$mail);
		$this->db->where('clients_password',$password);
		$this->db->where('role',$role);
		$data= $this->db->get();  
	     if($data->num_rows()>0) 
	     {
	     	return $data->result_array();
	     	
	     }
	     else
		{
			$this->session->set_flashdata('message','Invalid Email or Password ');

			redirect('LoginCI/login');

		}
	}
	public function developer_login($mail, $password,$role)
	{
		$this->db->select();
		$this->db->from('tem_add');
		$this->db->where('team_email',$mail);
		$this->db->where('team_password',$password);
		$this->db->where('role',$role);
		$data= $this->db->get();  
	     if($data->num_rows()>0) 
	     {
	     	return $data->result_array();
	     	
	     }
	     else
		{
			$this->session->set_flashdata('message','Invalid Email or Password ');

			redirect('LoginCI/login');

		}
	}
	public function admin_login($mail, $password,$role)
	{
		$this->db->select();
		$this->db->from($this->table_name);
		$this->db->where('user_email',$mail);
		$this->db->where('user_password',$password);
		$this->db->where('role',$role);
		$data= $this->db->get();  
	     if($data->num_rows()>0) 
	     {
	     	return $data->result_array();
	     	
	     }
	     else
		{
			$this->session->set_flashdata('message','Invalid Email or Password ');

			redirect('LoginCI/login');

		}
	}

	public  function  comparemail($email)
	{
		$this->db->where('user_email', $email);  
           $query = $this->db->get($this->table_name);  
           if($query->num_rows() > 0)  
           {  
                return true;  
           }  
           else  
           {  
                return false;  
           }  
             
	} 
	
}
?>