<?php

/**
* 
*/

include_once('Abstract_model.php');

class Project_details_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "projects";
		parent::__construct();
    }
    public function select_client()
    {
    	return $this->db->select()->from('clients')->get()->result_array();

    }
    public function select()
    {
    	return $this->db->select()->from($this->table_name)->get()->result_array();
    }
 	public function show_datatble($table)
  {
    
    $query = $this->db->get( $this->table_name );
    return $query->result(); 
  } 

  public function count_data()
  {
    $this->db->select();
    $this->db->from($this->table_name);
    return $this->db->count_all_results();
  }
  public function show_project_datatble($value)
  {
    $this->db->select("*");
    $this->db->from('clients');
    $this->db->join('projects','projects.client_id = clients.clients_id');
    // $this->db->where('role',$role);
    if(!empty($value))
      {
        $this->db->where(" (
          clients.clients_name like '%$value%' OR
          projects.project_title like '%$value%' OR
          projects.project_start_date like '%$value%' OR
          projects.project_end_date like '%$value%' OR
          projects.total_budget like '%$value%'
         
          ) ");
      }
    //$this->db->order_by('project_id','DESC');
    $query = $this->db->get();
    return $query->result(); 
  } 
  public function delete_client($column,$id)
  {
    $this->db->where($column,$id);
    $this->db->delete($this->table_name);
  }
  public function select_edit($id)
	{
		$this->db->where('project_id',$id);
		$data=$this->db->get( $this->table_name);
		return $data->result_array();
	}
   public function update_project_details($row_id, $data) 
  {
        $this->db->where('project_id', $row_id);
        return $this->db->update($this->table_name, $data);
    }

}

?>