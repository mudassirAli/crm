<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('Abstract_model.php');

class Clients_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
    $this->table_name = "clients";
		parent::__construct();
    }

    public function show_clients_project($id,$value)
    {
       $this->db->select('*');
       $this->db->from('projects');
       $this->db->join('clients','clients.clients_id=projects.client_id');
       $this->db->where('clients.clients_id',$id);
       if(!empty($value))
      {
        $this->db->where("(
          projects.project_title like '%$value%' OR
          projects.project_start_date like '%$value%' OR
          projects.project_end_date like '%$value%' 
          )");
      }
    $query = $this->db->get();
    return $query->result_array(); 
    }


    public function show_client_task($id,$projectid)
  {
       $this->db->select('*');
       $this->db->from('projects');
       $this->db->join('clients','clients.clients_id=projects.client_id');
       $this->db->where('clients.clients_id',$id);
       $this->db->where('projects.project_id',$projectid);
       $query = $this->db->get();
       return $query->result_array(); 
    // return $this->db->last_query(); 
  }
    
    
}