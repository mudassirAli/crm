<?php
function get_project_tasks($task_id){
	$CI = &get_instance();
	return $CI->db->get_where('tasks',['project_id'=>$task_id,'status'=>1])->result_array();

}

if (!function_exists('tasks_via_porject_id'))
{
   function tasks_via_porject_id($id){
	$CI = &get_instance();
	$CI->load->model('Devolper_model');
    $array_data = $CI->Devolper_model->get_tasks($id);
    return $array_data;
    
   }

}
