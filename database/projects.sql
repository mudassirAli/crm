-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2018 at 04:57 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `project_title` varchar(255) NOT NULL,
  `project_des` varchar(255) NOT NULL,
  `project_start_date` date NOT NULL,
  `project_end_date` date NOT NULL,
  `total_budget` varchar(255) NOT NULL,
  `budget_mile_stone` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `client_id`, `team_id`, `project_title`, `project_des`, `project_start_date`, `project_end_date`, `total_budget`, `budget_mile_stone`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 'asfddfasdf', 'asdfsdfasf', '0000-00-00', '0000-00-00', '50000', '34343', '2018-09-05 10:53:22', '0000-00-00 00:00:00'),
(2, 0, 0, 'asfddfasdf', 'asdfsdfasf', '0000-00-00', '0000-00-00', '50000', '34343', '2018-09-05 10:54:26', '0000-00-00 00:00:00'),
(3, 0, 0, 'asfddfasdf', 'asdfsdfasf', '0000-00-00', '0000-00-00', '50000', '34343', '2018-09-05 10:54:29', '0000-00-00 00:00:00'),
(4, 0, 0, 'asfddfasdf', 'asdfsdfasf', '0000-00-00', '0000-00-00', '50000', '34343', '2018-09-05 10:55:05', '0000-00-00 00:00:00'),
(12, 1, 0, 'marketing', 'testing  is done', '2018-09-06', '2018-09-26', '50000', '3434334', '2018-09-06 11:41:57', '2018-09-06 11:51:32'),
(13, 2, 0, 'Kenyon Stafford', 's est quia et earum ut in eius', '1992-10-03', '2006-11-13', '2234334', '70', '2018-09-06 12:47:03', '0000-00-00 00:00:00'),
(14, 3, 0, 'Patrick Greene', 'Irure molestias et ut Nam quia', '1971-10-14', '2001-11-19', '103434334334', '69', '2018-09-06 12:47:28', '0000-00-00 00:00:00'),
(16, 2, 0, 'John Miller', 'voluptatibus cupiditate dolor quis dignissimos adipisicing similique vero optio qui in lorem', '1979-06-28', '1983-12-28', '7423523545234', '94535345', '2018-09-06 13:09:40', '0000-00-00 00:00:00'),
(24, 2, 0, 'Barbara Sykes', 'Dolorem excepteur enim nihil quia consequat Tempore sunt hic voluptatem', '2010-05-08', '1998-05-24', '55', '15', '2018-09-06 13:11:37', '0000-00-00 00:00:00'),
(25, 1, 0, 'Shelley Campbell', 'asdafa', '2018-09-07', '2018-09-27', '23432', '1111', '2018-09-07 13:18:30', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
