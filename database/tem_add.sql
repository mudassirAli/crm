-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2018 at 04:57 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tem_add`
--

CREATE TABLE `tem_add` (
  `team_id` int(11) NOT NULL,
  `team_name` varchar(255) NOT NULL,
  `devlprtype_id` int(11) NOT NULL,
  `team_email` varchar(255) NOT NULL,
  `team_password` varchar(255) NOT NULL,
  `team_img` varchar(255) NOT NULL,
  `team_desgnation` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `datee` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tem_add`
--

INSERT INTO `tem_add` (`team_id`, `team_name`, `devlprtype_id`, `team_email`, `team_password`, `team_img`, `team_desgnation`, `role`, `datee`) VALUES
(10, 'imran', 1, 'developer@gmail.com', '123456789', 'beach-holidays5.jpg', 'sdsdfssdadada', 'developer', '2018-09-03'),
(12, 'Ali', 1, 'ad@gmail.com', 'gdgfdf', 'call_girl2.png', 'dfgdfg', 'developer', '2018-09-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tem_add`
--
ALTER TABLE `tem_add`
  ADD PRIMARY KEY (`team_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tem_add`
--
ALTER TABLE `tem_add`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
